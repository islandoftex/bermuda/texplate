tasks.create<Exec>("buildManual") {
  group = "documentation"
  description = "Compile the manual's TeX file to PDF."

  commandLine(listOf("arara", "-l", "-v", "texplate-manual.tex"))
  inputs.files("texplate-manual.tex")
  outputs.files("texplate-manual.pdf")
}
